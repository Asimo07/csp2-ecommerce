const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	userController.setAdmin(req.params).then(resultFromController => res.send(resultFromController));
})

router.get("/allUser", (req, res) => {
	userController.allData(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.placeOrder(userData, req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/order", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getOrder(userData).then(resultFromController => res.send(resultFromController));
})

router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.specificOrder(userData).then(resultFromController => res.send(resultFromController));
})


module.exports = router;