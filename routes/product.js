const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth")


router.get("/allProduct", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

router.post("/products", auth.verify, (req,res) =>{
	const data = auth.decode(req.headers.authorization);
	productController.addNewProduct(data, req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));	

})

router.put("/:productId/archieve", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)
	productController.archiveProduct(user, req.params).then(resultFromController => res.send(resultFromController));
	
});



module.exports = router;