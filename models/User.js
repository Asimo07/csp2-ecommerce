const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required: [true, "First Name is required"]
	},
	lastName : {
		type : String,
		required: [true, "Last Name is required"]
	},
	contact: {
		type : String,
		required: [true, "Number is required"]
	},
	email : {
		type : String,
		required: [true, "Email is required"]
	},
	password : {
		type : String,
		required: [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	myOrder : [{
		
		productId : {
			type: String,
			required: [true, "This is required"]
		},
		quantity: {
			type: String,
			required: [true, "This is required"]
		},
		orderOn: {
			type: Date,
			default: new Date()
		}
	}]

});	

module.exports = mongoose.model("User", userSchema);