const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type: Number,
		required: [true, "Amount is required"]
	},
	purhcasedOn : {
		type : Date,
		default: new Date()
	},
	orderDetails : [{
		userId : {
			type: String,
		},
		productId : {
			type: String,
		}	
	}]

	
});

module.exports = mongoose.model("Order", orderSchema);