const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return "Email already exist";
		}
		else{
			return "Invalid user";
		}
	})
}

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,	
		contact: reqBody.contact,	
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return "You have succefully register";
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return false;
			}

		}	
	})
};	

module.exports.setAdmin = (reqParams) => {
	let updateisAdmin = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateisAdmin).then((course,error) => {
		if(error){
			return false;
		}
		else{
			return "You are an Admin now!";
		}
	})
}

module.exports.allData = (data) => {
	return User.find(data).then(result => {
		return result;
	});
}

module.exports.placeOrder = async (userData, reqBody) => {
	if(userData.isAdmin === false){
		let userID = await User.findOne({_id: userData.id}).then(result => {
			return result.id;
		})

		let productID = await Product.findOne({_id: reqBody.productId}).then(result => {
			return result.id;
		})

		let price = await Product.findOne({_id: reqBody.productId}).then(result => {
			return result.price;
		})
		let newOrder = new Order({
			totalAmount: price * reqBody.quantity,
			orderDetails : [{
			   userId : userID,
			   productId : productID
			}]
		})

		User.findOne({_id: userData.id}).then(result => {
			let orderSummary = {
				productId: productID,
				quantity: reqBody.quantity
			}
			result.myOrder.push(orderSummary);
			return result.save().then((result, error) => {
				if(error){
					return `failed`;
				}else{
					return `success`;
				}
			})
		})

		Product.findOne({_id: reqBody.productId}).then(result => {
			result.whoOrder.push(userID);
			return result.save().then((result, error) => {
				if(error){
					return `failed`;
				}else{
					return `success`;
				}
			})
		})

		return newOrder.save().then((result, error) => {
			if(error){
				return `failed`;
			}else{
				return `success`;
			}
		})
	}else{
		return `You don't have access`;
	}
	

}

module.exports.getOrder = async (userData) => {
	if(userData.isAdmin){
		return Order.find({}).then(result => {
			return result;
		});
	}
	else{
		return `You do not have permission`;
	}
}	

module.exports.specificOrder = async (userData) => {
	if(userData.isAdmin === false){
		return Order.findOne({userId: userData.id}).then(result => {
			if (result == null){
				return `You have no order`;
			}
			else{
				return result;
			}
			
		});
	}
	else{
		return `Please Login`;
	}
}

