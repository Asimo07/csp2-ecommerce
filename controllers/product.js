const Product = require("../models/Product");


module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.addNewProduct = async (user, reqBody) => {
	if(user.isAdmin){
		let newItem = new Product({
			name: reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		})

		return newItem.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return "Product has been added";
			}
		})
	}
	else{
		return (`You have no access`);
	}
}

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return "Product has been updated";
			}
		})
	}else{
		return (`You have no access`);
	}
}

module.exports.archiveProduct = async (user, reqParams) => {

	if(user.isAdmin){
		let updateActiveField = {
			isActive : false
		};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {
			if (error) {
				return false;
			} else {
				return "You have succesfully archieve a product";
			}
		});
	}
	else{
		return (`You have no access`);
	}
	
};
