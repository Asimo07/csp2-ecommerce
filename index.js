const express = require("express")
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
/*const orderRoutes = require("./routes/order");*/

const app = express();

mongoose.connect("mongodb+srv://dbkimbryanvilloga:c6LHvb7Nvt7Saca9@wdc028-course-booking.pypbc.mongodb.net/csp2_ecommerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);

app.use("/products", productRoutes);

/*app.use("/orders", orderRoutes);*/

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});